const radioGroup = document.querySelector('.radio-group');
const links = document.querySelectorAll('.links a');

const cursorMagnetic = document.querySelector('.cursor-magnetic');
const cursorDot = document.querySelectorAll('.cursor-dot');
const cursorAnimate = document.querySelector('.cursor-animate');

const addMagneticHover = (e) => {
  cursorMagnetic.classList.add('fixed');
  cursorMagnetic.style.width = `${e.target.offsetWidth}px`;
  cursorMagnetic.style.height = `${e.target.offsetHeight}px`;

  const rect = e.target.getBoundingClientRect();
  cursorMagnetic.style.top = `${rect.top + window.scrollY}px`;
  cursorMagnetic.style.left = `${rect.left + window.scrollX}px`;
  cursorMagnetic.style.transform = 'none';
};

const removeMagneticHover = () => {
  cursorMagnetic.classList.remove('fixed');
  cursorMagnetic.removeAttribute('style');
};

links.forEach((link) => {
  link.addEventListener('mouseenter', addMagneticHover);
  link.addEventListener('mouseleave', removeMagneticHover);
});

document.addEventListener('click', () => {
  cursorAnimate.classList.add('expand');

  setTimeout(() => {
    cursorAnimate.classList.remove('expand');
  }, 2000);
});

radioGroup.addEventListener('change', () => {
  const checked = radioGroup.querySelector(':checked');

  [cursorMagnetic, cursorDot, cursorAnimate].forEach((cursor) => {
    if (cursor.length) {
      cursor.forEach((element) => { element.classList.remove('active'); });
    } else {
      cursor.classList.remove('active');
    }
  });

  switch (checked.value) {
    case 'magnetic':
      cursorMagnetic.classList.add('active');
      break;
    case 'dot':
      cursorDot.forEach((cursor) => { cursor.classList.add('active'); });
      break;
    case 'animate':
      cursorAnimate.classList.add('active');
      break;
    default:
      break;
  }
});

const lerp = (a, b, n) => (1 - n) * a + n * b;

class Cursor {
  constructor() {
    this.target = { x: 0.5, y: 0.5 };
    this.cursor = { x: 0.5, y: 0.5 };
    this.speed = 0.35;

    this.init();
  }

  bindAll() {
    // eslint-disable-next-line no-return-assign
    ['onmousemove', 'render'].forEach((fn) => (this[fn] = this[fn].bind(this)));
  }

  init() {
    this.bindAll();

    window.addEventListener('mousemove', this.onmousemove);
    this.raf = requestAnimationFrame(this.render);
  }

  onmousemove(e) {
    this.target.x = e.clientX / window.innerWidth;
    this.target.y = e.clientY / window.innerHeight;

    if (!this.raf) this.raf = requestAnimationFrame(this.render);
  }

  render() {
    this.cursor.x = lerp(this.cursor.x, this.target.x, this.speed);
    this.cursor.y = lerp(this.cursor.y, this.target.y, this.speed);

    document.documentElement.style.setProperty('--cursor-x', this.cursor.x);
    document.documentElement.style.setProperty('--cursor-y', this.cursor.y);

    const delta = Math.sqrt(
      ((this.target.x - this.cursor.x) ** 2)
      + ((this.target.y - this.cursor.y) ** 2),
    );

    if (delta < 0.001) {
      cancelAnimationFrame(this.raf);
      this.raf = null;
      return;
    }

    this.raf = requestAnimationFrame(this.render);
  }
}

// eslint-disable-next-line no-new
new Cursor();
